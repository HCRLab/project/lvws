<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="description"
        content="Learning for Dynamic Subteaming and Voluntary Waiting solves the multi-robot collaborative scheduling problem with a graph attention transformer network.">
  <meta name="keywords" content="LVWS, multi-robot collaborative scheduling">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Learning for Dynamic Subteaming and Voluntary Waiting</title>

  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-PYVRSFMDRL');
  </script>

  <link href="https://fonts.googleapis.com/css?family=Google+Sans|Noto+Sans|Castoro"
        rel="stylesheet">

  <link rel="stylesheet" href="./static/css/bulma.min.css">
  <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/gh/jpswalsh/academicons@1/css/academicons.min.css">
  <link rel="stylesheet" href="./static/css/index.css">
  <link rel="icon" href="./static/images/favicon.ico">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>


<section class="hero">
  <div class="hero-body">
    <div class="container is-max-desktop">
      <div class="columns is-centered">
        <div class="column has-text-centered">
          <h1 class="title is-1 publication-title">Learning for Dynamic Subteaming and Voluntary Waiting in Heterogeneous Multi-Robot Collaborative Scheduling</h1>
          <div class="is-size-5 publication-authors">
            <span class="author-block">
              Williard Joshua Jose and</span>
            <span class="author-block">
              Hao Zhang</span>
          </div>

          <div class="is-size-5 publication-authors">
            <span class="author-block">Human-Centered Robotics Laboratory @ UMass Amherst</span>
          </div>

          <div class="column has-text-centered">
            <span class="link-block">
              <a class="external-link" href="https://hcr.cs.umass.edu">
                  <img src="./static/images/hcrlab-logo.png" alt="HCR Lab logo" 
                        width="200" />
              </a>
            </span>
          </div>

          <div class="column has-text-centered">
            <div class="publication-links">
              <!-- PDF Link. -->
              <span class="link-block">
                <a href="./static/pdf/ICRA24-LVWS.pdf"
                   class="external-link button is-normal is-rounded is-dark">
                  <span class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                      <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m2.25 0H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                    </svg>
                  </span>
                  <span>Paper</span>
                </a>
              </span>
              <!-- Video Link. -->
              <span class="link-block">
                <!-- <a href="https://www.youtube.com/watch?v=zslbOXQXtSI" -->
                <a href="./static/videos/presentation.mp4"
                  class="external-link button is-normal is-rounded is-dark">
                  <span class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                      <path stroke-linecap="round" stroke-linejoin="round" d="M5.25 5.653c0-.856.917-1.398 1.667-.986l11.54 6.347a1.125 1.125 0 0 1 0 1.972l-11.54 6.347a1.125 1.125 0 0 1-1.667-.986V5.653Z" />
                    </svg>
                  </span>
                  <span>Video</span>
                </a>
              </span>
            </div>
          </div>

          <div class="column has-text-centered">
            <div class="is-size-5 publication-venue">
                IEEE International Conference on Robotics and Automation (ICRA) 2024
            </div>
            <div class="is-size-5 publication-awards">
              Best Paper Award Finalist on Multi-Robot Systems
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="section">
  <div class="container is-max-desktop">
    <!-- Abstract. -->
    <div class="columns is-centered has-text-centered">
      <div class="column is-max-desktop">
        <h2 class="title is-3">Abstract</h2>
        <div class="content has-text-justified">
          <p>
            Coordinating heterogeneous robots is essential for autonomous multi-robot teaming. 
            To execute a set of dependent tasks as quickly as possible,
            and to complete tasks that cannot be addressed by individual robots,
            it is necessary to form subteams that can collaboratively finish the tasks.
            It is also advantageous for robots to 
            wait for teammates and tasks to become available
            in order to form better subteams or reduce the overall completion time.
          </p>
          <p>
            To enable both abilities,
            we introduce a new graph learning approach 
            that formulates heterogeneous collaborative scheduling as a bipartite matching problem that maximizes a reward matrix learned via imitation learning. 
            We design a novel graph attention transformer network (GATN) 
            that represents the problem of collaborative scheduling as a bipartite graph,
            and integrates both local and global graph information  to estimate the reward matrix using graph attention networks and transformers.
          </p>
          <p>
            By relaxing the constraint of one-to-one correspondence in  bipartite matching,
            our approach allows multiple robots to address the same task as a subteam. 
            Our approach also enables voluntary waiting by introducing an idle task that the robots can select to wait. 
            Experimental results have shown that our approach well addresses heterogeneous collaborative scheduling with dynamic subteam formation and voluntary waiting,
            and outperforms the previous and baseline methods.
          </p>
        </div>
      </div>
    </div>
    <!--/ Abstract. -->

    <!-- Paper video. -->
    <div class="columns is-centered has-text-centered">
      <div class="column  is-four-fifths">
        <h2 class="title is-3">Video</h2>
        <!-- <div class="publication-video">
          <iframe src="https://www.youtube.com/embed/zslbOXQXtSI?rel=0&amp;showinfo=0"
                  frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div> -->
        <video controls playsinline allowfullscreen width="100%">
          <source src="./static/videos/presentation.mp4"
                  type="video/mp4">
        </video>
      </div>
    </div>
    <!--/ Paper video. -->
  </div>
</section>


<section class="section">
  <div class="container is-max-desktop">
    <!-- Motivation. -->
    <div class="columns is-centered">
      <div class="column is-four-fifths">
        <div class="content">
          <div class="has-text-centered">
            <img src="./static/images/motivation.svg"
                  width="100%" />
          </div>
          <p>A motivating scenario for heterogeneous multi-robot collaborative 
            scheduling with dynamic subteaming and voluntary waiting in an 
            assembly application: Subteaming allows robots to dynamically build 
            subteams to address tasks that cannot be completed by individual robots,
            while voluntary waiting enables robots to wait for additional robots 
            and tasks to be available in order to form better subteams or reduce the 
            overall task completion time.</p>
        </div>
        <br/>
        <!--/ Interpolating. -->

      </div>
    </div>
    <!-- Approach. -->
    <div class="columns is-centered">
      <div class="column is-four-fifths">
        <div class="content">
          <h3 class="title is-4">Learning for Voluntary Waiting and Subteaming (LVWS)</h3>
          <div class="has-text-centered">
            <img src="./static/images/approach.svg"
                  width="100%" />
          </div>
          <p>Overview of our LVWS approach:
            By maximizing a scheduling reward matrix R that is estimated using imitation learning,
            LVWS integrates GATs and transformers to learn a policy to determine robot-task assignments.
            LVWS enables subteaming by grouping multiple robots that collectively meet a task's requirements on capabilities and payload capacities.
            LVWS enables voluntary waiting by allowing robots to select the idle task t<sub>M+1</sub>.</p>
        </div>
        <br/>
        <!--/ Interpolating. -->

      </div>
    </div>
    <!--/ Animation. -->



    <!-- Demo Videos -->
    <h2 class="title is-3">Demo Videos</h2>
    <div class="columns is-centered">

      <!-- Simulation -->
      <div class="column">
        <div class="content">
          <h3 class="title is-4">Simulation (5x speed)</h3>
          <video autoplay controls muted loop playsinline height="100%">
            <source src="./static/videos/demo-simulation-5x.mp4"
                    type="video/mp4">
          </video>
          <p>
            We demonstrate our method in a manufacturing assembly case study run in a Gazebo simulation. 
            There are 3 robot manipulators in 3 assembly cells, and there are also 3 ground vehicles with different payload capacities. 
            The manufacturing process is decomposed into 14 tasks with varying capability and payload requirements.
          </p>
          <p>
            We can see one of the jackal robots demonstrating voluntary waiting. 
            We can also see the husky and jackal robot has formed a subteam to transport an oversized payload. 
            These robots are autonomously operating according to the multi-robot task scheduling solution that was computed by LVWS. 
            For this specific case study, the solution by LVWS is also the exact solution. 
          </p>
        </div>
      </div>
      <!--/ Simulation -->

      <!-- Real World -->
      <div class="column">
        <h3 class="title is-4">Real World (5x speed)</h3>
        <div class="columns is-centered">
          <div class="column content">
            <video autoplay controls muted loop playsinline height="100%">
              <source src="./static/videos/demo-realworld-5x.mp4"
                      type="video/mp4">
            </video>
            <p>
              We can also demonstrate our method running on real-world robots. 
              We have 2 human workers in assembly cells replacing the robot manipulators, and we have the same 3 ground vehicles. 
              The simplified manufacturing process is decomposed into 7 tasks.
            </p>
            <p>
              We also observed one of the jackals demonstrating voluntary waiting at the start. 
              The two jackals also successfully transported an oversized payload by forming a subteam. 
              The robots here are being teleoperated, but still according to the scheduling solution by LVWS, which is also the optimal solution for this case. 
            </p>
          </div>

        </div>
      </div>
    </div>
    <!--/ Demo Videos. -->

  </div>
</section>


<section class="section" id="BibTeX">
  <div class="container is-max-desktop content">
    <h2 class="title">BibTeX</h2>
    <pre><code>@inproceedings{jose2024lvws,
  author    = {Jose, Williard Joshua and Zhang, Hao},
  title     = {Learning for Dynamic Subteaming and Voluntary Waiting in Heterogeneous Multi-Robot Collaborative Scheduling},
  booktitle = {IEEE International Conference on Robotics and Automation},
  year      = {2024}
}</code></pre>
  </div>
</section>


<footer class="footer">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-8">
        <div class="content">
          <p>
            This work was partially supported by the DARPA Young Faculty Award (YFA) D21AP10114-00 and the NSF CAREER Award IIS-2308492.
          </p>
          <p>
            This website is adapted from <a href="https://github.com/nerfies/nerfies.github.io">Nerfies</a>, licensed
            under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative
            Commons Attribution-ShareAlike 4.0 International License</a>.
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>

</body>
</html>
